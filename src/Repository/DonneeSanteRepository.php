<?php

namespace App\Repository;

use App\Entity\DonneeSante;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DonneeSante|null find($id, $lockMode = null, $lockVersion = null)
 * @method DonneeSante|null findOneBy(array $criteria, array $orderBy = null)
 * @method DonneeSante[]    findAll()
 * @method DonneeSante[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DonneeSanteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DonneeSante::class);
    }

    // /**
    //  * @return DonneeSante[] Returns an array of DonneeSante objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DonneeSante
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
