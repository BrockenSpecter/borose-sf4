<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191101180131 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE laboratoire (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, complement_adresse VARCHAR(255) NOT NULL, cp VARCHAR(5) NOT NULL, ville VARCHAR(100) NOT NULL, nom_contact VARCHAR(100) DEFAULT NULL, prenom_contact VARCHAR(100) DEFAULT NULL, email_contact VARCHAR(255) DEFAULT NULL, tel_contact VARCHAR(20) DEFAULT NULL, dt_crea DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE donnee_sante (id INT AUTO_INCREMENT NOT NULL, patient_id INT NOT NULL, pays_id INT NOT NULL, laboratoire_id INT NOT NULL, dep VARCHAR(10) NOT NULL, md_spe VARCHAR(255) DEFAULT NULL, institution VARCHAR(255) DEFAULT NULL, indic VARCHAR(255) DEFAULT NULL, nominal_dose VARCHAR(255) DEFAULT NULL, actual_dose VARCHAR(255) DEFAULT NULL, nominal_dtime VARCHAR(255) DEFAULT NULL, actual_dtime VARCHAR(255) DEFAULT NULL, concentration VARCHAR(255) DEFAULT NULL, sex VARCHAR(255) DEFAULT NULL, wt VARCHAR(255) DEFAULT NULL, age VARCHAR(255) DEFAULT NULL, crcl VARCHAR(255) DEFAULT NULL, ir_cat VARCHAR(255) DEFAULT NULL, ast VARCHAR(255) DEFAULT NULL, alt VARCHAR(255) DEFAULT NULL, ih_cat VARCHAR(255) DEFAULT NULL, dose_freq VARCHAR(5) DEFAULT NULL, actual_stime VARCHAR(10) DEFAULT NULL, INDEX IDX_7D552FDA6B899279 (patient_id), INDEX IDX_7D552FDAA6E44244 (pays_id), INDEX IDX_7D552FDA76E2617B (laboratoire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pays (id INT AUTO_INCREMENT NOT NULL, laboratoire_id INT DEFAULT NULL, code VARCHAR(2) NOT NULL, name VARCHAR(20) NOT NULL, INDEX IDX_349F3CAE76E2617B (laboratoire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient (id INT AUTO_INCREMENT NOT NULL, pays_id INT NOT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, date_naissance DATE NOT NULL, sexe INT NOT NULL, adresse VARCHAR(255) NOT NULL, complement_adresse VARCHAR(255) NOT NULL, com INT NOT NULL, dep VARCHAR(3) NOT NULL, cp VARCHAR(5) NOT NULL, ville VARCHAR(100) NOT NULL, tel_contact VARCHAR(20) DEFAULT NULL, tel_mobile VARCHAR(20) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1ADAD7EBA6E44244 (pays_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE donnee_sante ADD CONSTRAINT FK_7D552FDA6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE donnee_sante ADD CONSTRAINT FK_7D552FDAA6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id)');
        $this->addSql('ALTER TABLE donnee_sante ADD CONSTRAINT FK_7D552FDA76E2617B FOREIGN KEY (laboratoire_id) REFERENCES laboratoire (id)');
        $this->addSql('ALTER TABLE pays ADD CONSTRAINT FK_349F3CAE76E2617B FOREIGN KEY (laboratoire_id) REFERENCES laboratoire (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EBA6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE donnee_sante DROP FOREIGN KEY FK_7D552FDA76E2617B');
        $this->addSql('ALTER TABLE pays DROP FOREIGN KEY FK_349F3CAE76E2617B');
        $this->addSql('ALTER TABLE donnee_sante DROP FOREIGN KEY FK_7D552FDAA6E44244');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EBA6E44244');
        $this->addSql('ALTER TABLE donnee_sante DROP FOREIGN KEY FK_7D552FDA6B899279');
        $this->addSql('DROP TABLE laboratoire');
        $this->addSql('DROP TABLE donnee_sante');
        $this->addSql('DROP TABLE pays');
        $this->addSql('DROP TABLE patient');
    }
}
