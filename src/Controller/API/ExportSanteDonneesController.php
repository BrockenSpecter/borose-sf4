<?php

namespace App\Controller\API;

use App\Repository\DonneeSanteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class ExportSanteDonneesController extends AbstractController
{
    const HEADER = [
        'Nom','Prenom','Pays','Labo','Dept','MdSpe','Institution','Indic','NominalDose','ActualDose','NominalDTime','ActualDTime','DoseFreq','ActualSTime','Concentration','Sex','Wt','Age','Crcl','IrCat','Ast','Alt','IhCat'
    ];

    /**
     * @Route("/api/export/donnees", name="api_sante_export")
     *
     * @param DonneeSanteRepository $donneeSanteRepository
     *
     * @return StreamedResponse
     */
    public function csv(DonneeSanteRepository $donneeSanteRepository)
    {
        $response = new StreamedResponse(function () use ($donneeSanteRepository) {
            $handle = fopen('php://output', 'r+');
            $all = $donneeSanteRepository->findAll();

            fputcsv($handle, self::HEADER);

            foreach ($all as $value) {
                fputcsv($handle, [
                    $value->getPatient()->getNom(),
                    $value->getPatient()->getPrenom(),
                    $value->getPatient()->getPays()->getName(),
                    $value->getLaboratoire()->getNom(),
                    $value->getDep(),
                    $value->getMdSpe(),
                    $value->getInstitution(),
                    $value->getIndic(),
                    $value->getNominalDose(),
                    $value->getActualDose(),
                    $value->getNominalDTime(),
                    $value->getActualDTime(),
                    $value->getDoseFreq(),
                    $value->getActualSTime(),
                    $value->getConcentration(),
                    $value->getSex(),
                    $value->getWt(),
                    $value->getAge(),
                    $value->getCrcl(),
                    $value->getIrCat(),
                    $value->getAst(),
                    $value->getAlt(),
                    $value->getIhCat()
                ]);
            }
            fclose($handle);
        });

        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export_donnees_sante'.date('dmY').'.csv"');

        return $response;
    }
}
