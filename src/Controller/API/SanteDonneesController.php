<?php


namespace App\Controller\API;

use App\Entity\DonneeSante;
use App\Repository\LaboratoireRepository;
use App\Repository\PatientRepository;
use App\Repository\PaysRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SanteDonneesController extends AbstractController
{
    /**
     * @Route("/api/import/donnees", name="api_sante_import")
     *
     * @param EntityManagerInterface $em
     * @param PaysRepository $paysRepository
     * @param PatientRepository $patientRepository
     * @param LaboratoireRepository $laboratoireRepository
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function import(
        EntityManagerInterface $em,
        PaysRepository $paysRepository,
        PatientRepository $patientRepository,
        LaboratoireRepository $laboratoireRepository
    ) {
        $json = json_decode(file_get_contents(__DIR__ . '/../../Bouchon/imitox.json'), true);

        foreach ($json as $value) {
            $pays = $paysRepository->findOneByCode($value['IdCountry']);
            $patient = $patientRepository->find($value['IdPatient']);
            $labo = $laboratoireRepository->findOneByNom($value['IdLab']);

            $donnee = new DonneeSante();
            $donnee = $donnee->setDataFromJson($pays, $patient, $labo, $value);
            $em->persist($donnee);
        }
        $em->flush();

        return $this->json('ok');
    }
}
