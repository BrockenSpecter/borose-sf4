<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LaboratoireRepository")
 */
class Laboratoire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $complementAdresse;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity="Pays", mappedBy="laboratoire", orphanRemoval=true)
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nomContact;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $prenomContact;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailContact;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telContact;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dtCrea;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DonneeSante", mappedBy="laboratoire")
     */
    private $donneeSantes;

    public function __construct()
    {
        $this->pays = new ArrayCollection();
        $this->donneeSantes = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Laboratoire
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string|null
     */
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse(string $adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string|null
     */
    public function getComplementAdresse(): ?string
    {
        return $this->complementAdresse;
    }

    /**
     * @param string $complementAdresse
     */
    public function setComplementAdresse(string $complementAdresse): void
    {
        $this->complementAdresse = $complementAdresse;
    }

    /**
     * @return string|null
     */
    public function getCp(): ?string
    {
        return $this->cp;
    }

    /**
     * @param string $cp
     */
    public function setCp(string $cp): void
    {
        $this->cp = $cp;
    }

    /**
     * @return string|null
     */
    public function getVille(): ?string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille(string $ville): void
    {
        $this->ville = $ville;
    }

    /**
     * @return Collection|Pays[]
     */
    public function getPays(): Collection
    {
        return $this->pays;
    }

    /**
     * @param Pays $pays
     */
    public function addPay(Pays $pays): void
    {
        if (!$this->pays->contains($pays)) {
            $this->pays[] = $pays;
            $pays->setLaboratoire($this);
        }
    }

    /**
     * @param Pays $pays
     */
    public function removePay(Pays $pays): void
    {
        if ($this->pays->contains($pays)) {
            $this->pays->removeElement($pays);
            // set the owning side to null (unless already changed)
            if ($pays->getLaboratoire() === $this) {
                $pays->setLaboratoire(null);
            }
        }
    }

    /**
     * @return string|null
     */
    public function getNomContact(): ?string
    {
        return $this->nomContact;
    }

    /**
     * @param string $nomContact
     */
    public function setNomContact(string $nomContact): void
    {
        $this->nomContact = $nomContact;
    }

    /**
     * @return string|null
     */
    public function getPrenomContact(): ?string
    {
        return $this->prenomContact;
    }

    /**
     * @param string $prenomContact
     */
    public function setPrenomContact(string $prenomContact): void
    {
        $this->prenomContact = $prenomContact;
    }

    /**
     * @return string|null
     */
    public function getEmailContact(): ?string
    {
        return $this->emailContact;
    }

    /**
     * @param string|null $emailContact
     */
    public function setEmailContact(?string $emailContact): void
    {
        $this->emailContact = $emailContact;
    }

    /**
     * @return string|null
     */
    public function getTelContact(): ?string
    {
        return $this->telContact;
    }

    /**
     * @param string|null $telContact
     */
    public function setTelContact(?string $telContact): void
    {
        $this->telContact = $telContact;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDtCrea(): ?\DateTimeInterface
    {
        return $this->dtCrea;
    }

    /**
     * @param \DateTimeInterface $dtCrea
     */
    public function setDtCrea(\DateTimeInterface $dtCrea): void
    {
        $this->dtCrea = $dtCrea;
    }

    /**
     * @return Collection|DonneeSante[]
     */
    public function getDonneeSantes(): Collection
    {
        return $this->donneeSantes;
    }

    public function addDonneeSante(DonneeSante $donneeSante): self
    {
        if (!$this->donneeSantes->contains($donneeSante)) {
            $this->donneeSantes[] = $donneeSante;
            $donneeSante->setLaboratoire($this);
        }

        return $this;
    }

    public function removeDonneeSante(DonneeSante $donneeSante): self
    {
        if ($this->donneeSantes->contains($donneeSante)) {
            $this->donneeSantes->removeElement($donneeSante);
            // set the owning side to null (unless already changed)
            if ($donneeSante->getLaboratoire() === $this) {
                $donneeSante->setLaboratoire(null);
            }
        }

        return $this;
    }
}
