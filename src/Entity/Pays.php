<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaysRepository")
 */
class Pays
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=2)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Laboratoire", inversedBy="pays")
     * @ORM\JoinColumn(nullable=true)
     */
    private $laboratoire;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DonneeSante", mappedBy="pays")
     */
    private $donneeSantes;

    public function __construct()
    {
        $this->donneeSantes = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return Laboratoire|null
     */
    public function getLaboratoire(): ?Laboratoire
    {
        return $this->laboratoire;
    }

    /**
     * @param Laboratoire|null $laboratoire
     */
    public function setLaboratoire(?Laboratoire $laboratoire): void
    {
        $this->laboratoire = $laboratoire;
    }

    /**
     * @return Collection|DonneeSante[]
     */
    public function getDonneeSantes(): Collection
    {
        return $this->donneeSantes;
    }

    public function addDonneeSante(DonneeSante $donneeSante): self
    {
        if (!$this->donneeSantes->contains($donneeSante)) {
            $this->donneeSantes[] = $donneeSante;
            $donneeSante->setPays($this);
        }

        return $this;
    }

    public function removeDonneeSante(DonneeSante $donneeSante): self
    {
        if ($this->donneeSantes->contains($donneeSante)) {
            $this->donneeSantes->removeElement($donneeSante);
            // set the owning side to null (unless already changed)
            if ($donneeSante->getPays() === $this) {
                $donneeSante->setPays(null);
            }
        }

        return $this;
    }
}
