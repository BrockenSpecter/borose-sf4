<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prenom;

    /**
     * @ORM\Column(type="date")
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="integer")
     */
    private $sexe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementAdresse;

    /**
     * @ORM\OneToOne(targetEntity="Pays")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pays;

    /**
     * @ORM\Column(type="integer")
     */
    private $com;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $dep;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telContact;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telMobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DonneeSante", mappedBy="patient")
     */
    private $donneeSantes;

    public function __construct()
    {
        $this->donneeSantes = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string|null
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    /**
     * @param \DateTimeInterface $dateNaissance
     */
    public function setDateNaissance(\DateTimeInterface $dateNaissance): void
    {
        $this->dateNaissance = $dateNaissance;
    }

    /**
     * @return int|null
     */
    public function getSexe(): ?int
    {
        return $this->sexe;
    }

    /**
     * @param int $sexe
     */
    public function setSexe(int $sexe): void
    {
        $this->sexe = $sexe;
    }

    /**
     * @return string|null
     */
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse(string $adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string|null
     */
    public function getComplementAdresse(): ?string
    {
        return $this->complementAdresse;
    }

    /**
     * @param string $complementAdresse
     */
    public function setComplementAdresse(string $complementAdresse): void
    {
        $this->complementAdresse = $complementAdresse;
    }

    /**
     * @return Pays|null
     */
    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    /**
     * @param Pays $pays
     */
    public function setPays(Pays $pays): void
    {
        $this->pays = $pays;
    }

    /**
     * @return int|null
     */
    public function getCom(): ?int
    {
        return $this->com;
    }

    /**
     * @param int $com
     */
    public function setCom(int $com): void
    {
        $this->com = $com;
    }

    /**
     * @return string|null
     */
    public function getDep(): ?string
    {
        return $this->dep;
    }

    /**
     * @param string $dep
     */
    public function setDep(string $dep): void
    {
        $this->dep = $dep;
    }

    /**
     * @return string|null
     */
    public function getCp(): ?string
    {
        return $this->cp;
    }

    /**
     * @param string $cp
     */
    public function setCp(string $cp): void
    {
        $this->cp = $cp;
    }

    /**
     * @return string|null
     */
    public function getVille(): ?string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille(string $ville): void
    {
        $this->ville = $ville;
    }

    /**
     * @return string|null
     */
    public function getTelContact(): ?string
    {
        return $this->telContact;
    }

    /**
     * @param string $telContact
     */
    public function setTelContact(string $telContact): void
    {
        $this->telContact = $telContact;
    }

    /**
     * @return string|null
     */
    public function getTelMobile(): ?string
    {
        return $this->telMobile;
    }

    /**
     * @param string|null $telMobile
     */
    public function setTelMobile(?string $telMobile): void
    {
        $this->telMobile = $telMobile;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     */
    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return Collection|DonneeSante[]
     */
    public function getDonneeSantes(): Collection
    {
        return $this->donneeSantes;
    }

    public function addDonneeSante(DonneeSante $donneeSante): self
    {
        if (!$this->donneeSantes->contains($donneeSante)) {
            $this->donneeSantes[] = $donneeSante;
            $donneeSante->setPatient($this);
        }

        return $this;
    }

    public function removeDonneeSante(DonneeSante $donneeSante): self
    {
        if ($this->donneeSantes->contains($donneeSante)) {
            $this->donneeSantes->removeElement($donneeSante);
            // set the owning side to null (unless already changed)
            if ($donneeSante->getPatient() === $this) {
                $donneeSante->setPatient(null);
            }
        }

        return $this;
    }
}
