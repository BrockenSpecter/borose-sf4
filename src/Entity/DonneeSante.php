<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DonneeSanteRepository")
 */
class DonneeSante
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $dep;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mdSpe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $indic;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nominalDose;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actualDose;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nominalDTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actualDTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $concentration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sex;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crcl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $irCat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ast;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ihCat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient", inversedBy="donneeSantes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pays", inversedBy="donneeSantes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pays;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Laboratoire", inversedBy="donneeSantes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $laboratoire;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $doseFreq;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $actualSTime;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDep(): ?string
    {
        return $this->dep;
    }

    /**
     * @param string $dep
     */
    public function setDep(string $dep): void
    {
        $this->dep = $dep;
    }

    /**
     * @return string|null
     */
    public function getMdSpe(): ?string
    {
        return $this->mdSpe;
    }

    /**
     * @param string|null $mdSpe
     */
    public function setMdSpe(?string $mdSpe): void
    {
        $this->mdSpe = $mdSpe;
    }

    /**
     * @return string|null
     */
    public function getInstitution(): ?string
    {
        return $this->institution;
    }

    /**
     * @param string|null $institution
     */
    public function setInstitution(?string $institution): void
    {
        $this->institution = $institution;
    }

    /**
     * @return string|null
     */
    public function getIndic(): ?string
    {
        return $this->indic;
    }

    /**
     * @param string|null $indic
     */
    public function setIndic(?string $indic): void
    {
        $this->indic = $indic;
    }

    /**
     * @return string|null
     */
    public function getNominalDose(): ?string
    {
        return $this->nominalDose;
    }

    /**
     * @param string|null $nominalDose
     */
    public function setNominalDose(?string $nominalDose): void
    {
        $this->nominalDose = $nominalDose;
    }

    /**
     * @return string|null
     */
    public function getActualDose(): ?string
    {
        return $this->actualDose;
    }

    /**
     * @param string|null $actualDose
     */
    public function setActualDose(?string $actualDose): void
    {
        $this->actualDose = $actualDose;
    }

    /**
     * @return string|null
     */
    public function getNominalDTime(): ?string
    {
        return $this->nominalDTime;
    }

    /**
     * @param string|null $nominalDTime
     */
    public function setNominalDTime(?string $nominalDTime): void
    {
        $this->nominalDTime = $nominalDTime;
    }

    /**
     * @return string|null
     */
    public function getActualDTime(): ?string
    {
        return $this->actualDTime;
    }

    /**
     * @param string|null $actualDTime
     */
    public function setActualDTime(?string $actualDTime): void
    {
        $this->actualDTime = $actualDTime;
    }

    /**
     * @return string|null
     */
    public function getConcentration(): ?string
    {
        return $this->concentration;
    }

    /**
     * @param string|null $concentration
     */
    public function setConcentration(?string $concentration): void
    {
        $this->concentration = $concentration;
    }

    /**
     * @return string|null
     */
    public function getSex(): ?string
    {
        return $this->sex;
    }

    /**
     * @param string|null $sex
     */
    public function setSex(?string $sex): void
    {
        $this->sex = $sex;
    }

    /**
     * @return string|null
     */
    public function getWt(): ?string
    {
        return $this->wt;
    }

    /**
     * @param string|null $wt
     */
    public function setWt(?string $wt): void
    {
        $this->wt = $wt;
    }

    /**
     * @return string|null
     */
    public function getAge(): ?string
    {
        return $this->age;
    }

    /**
     * @param string|null $age
     */
    public function setAge(?string $age): void
    {
        $this->age = $age;
    }

    /**
     * @return string|null
     */
    public function getCrcl(): ?string
    {
        return $this->crcl;
    }

    /**
     * @param string|null $crcl
     */
    public function setCrcl(?string $crcl): void
    {
        $this->crcl = $crcl;
    }

    /**
     * @return string|null
     */
    public function getIrCat(): ?string
    {
        return $this->irCat;
    }

    /**
     * @param string|null $irCat
     */
    public function setIrCat(?string $irCat): void
    {
        $this->irCat = $irCat;
    }

    /**
     * @return string|null
     */
    public function getAst(): ?string
    {
        return $this->ast;
    }

    /**
     * @param string|null $ast
     */
    public function setAst(?string $ast): void
    {
        $this->ast = $ast;
    }

    /**
     * @return string|null
     */
    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string|null $alt
     */
    public function setAlt(?string $alt): void
    {
        $this->alt = $alt;
    }

    /**
     * @return string|null
     */
    public function getIhCat(): ?string
    {
        return $this->ihCat;
    }

    /**
     * @param string|null $ihCat
     */
    public function setIhCat(?string $ihCat): void
    {
        $this->ihCat = $ihCat;
    }

    /**
     * @return Patient|null
     */
    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    /**
     * @param Patient|null $patient
     */
    public function setPatient(?Patient $patient): void
    {
        $this->patient = $patient;
    }

    /**
     * @return Pays|null
     */
    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    /**
     * @param Pays|null $pays
     */
    public function setPays(?Pays $pays): void
    {
        $this->pays = $pays;
    }

    /**
     * @return Laboratoire|null
     */
    public function getLaboratoire(): ?Laboratoire
    {
        return $this->laboratoire;
    }

    /**
     * @param Laboratoire|null $laboratoire
     */
    public function setLaboratoire(?Laboratoire $laboratoire): void
    {
        $this->laboratoire = $laboratoire;
    }

    /**
     * @return string|null
     */
    public function getDoseFreq(): ?string
    {
        return $this->doseFreq;
    }

    /**
     * @param string|null $doseFreq
     */
    public function setDoseFreq(?string $doseFreq): void
    {
        $this->doseFreq = $doseFreq;
    }

    /**
     * @return string|null
     */
    public function getActualSTime(): ?string
    {
        return $this->actualSTime;
    }

    /**
     * @param string|null $actualSTime
     */
    public function setActualSTime(?string $actualSTime): void
    {
        $this->actualSTime = $actualSTime;
    }

    /**
     * @param $pays
     * @param $patient
     * @param $labo
     * @param $value
     *
     * @return $this
     */
    public function setDataFromJson($pays, $patient, $labo, $value): self
    {
        $this->setPays($pays);
        $this->setDep($value['IdDept']);
        $this->setLaboratoire($labo);
        $this->setMdSpe($value['MdSpe']);
        $this->setInstitution($value['Institution']);
        $this->setIndic($value['Indic']);
        $this->setNominalDose($value['NominalDose']);
        $this->setActualDose($value['ActualDose']);
        $this->setNominalDTime($value['NominalDTime']);
        $this->setActualDTime($value['ActualDTime']);
        $this->setDoseFreq($value['DoseFreq']);
        $this->setActualSTime($value['ActualSTime']);
        $this->setConcentration($value['Concentration']);
        $this->setSex($value['Sex']);
        $this->setWt($value['Wt']);
        $this->setAge($value['Age']);
        $this->setCrcl($value['Crcl']);
        $this->setIrCat($value['IrCat']);
        $this->setAst($value['Ast']);
        $this->setAlt($value['Alt']);
        $this->setIhCat($value['IhCat']);
        $this->setPatient($patient);

        return $this;
    }
}
