FROM php:7.3-apache

MAINTAINER Kristopher Perin <kperin@mediametrie.fr>

RUN apt-get update \
    && apt-get install -y wget git curl apt-transport-https gnupg gnupg2 gnupg1 zip unzip apt-utils vim nano

RUN wget http://pecl.php.net/get/xdebug-2.7.2.tgz \
    && pear install xdebug-2.7.2.tgz

RUN docker-php-ext-install mbstring pdo pdo_mysql
RUN docker-php-ext-enable xdebug

ADD . /var/www/html/
RUN chown -R www-data:www-data /var/www/html

RUN echo "127.0.0.1 borose.local" >> /etc/hosts

RUN a2enmod rewrite

WORKDIR /var/www/html/

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && HASH="$(wget -q -O - https://composer.github.io/installer.sig)" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \

ADD ./docker/borose.conf /etc/apache2/sites-enabled/

RUN echo 'memory_limit = 512M' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini
